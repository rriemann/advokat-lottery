# P2P Lottery

This code is largely based on [Kad](https://github.com/gordonwritescode/kad) and the [WebRTC Transport](https://github.com/kadtools/kad-webrtc) for Kad.

## Setup

- `npm install`
- `npm run build-lottery`

## Run

- open <http://localhost:8080/index.html> in one tab
- open the link from there somewhere else
