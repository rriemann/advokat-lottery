'use strict';

// kademlia + transport
var kademlia = require('kad');
var EventEmitter = require('events').EventEmitter;
var WebRTC = require('./transport');

// advokat
var Aggregator = require('./advokat/aggregator');
var akConstants = require('./advokat/constants');
kademlia.constants.T_RESPONSETIMEOUT = 5*1000; // in ms
kademlia.constants.MESSAGE_TYPES.push(...akConstants.MESSAGE_TYPES);


// angular
var app = angular.module('LotteryApp', ['timer']);


app.controller('LotteryController', function($scope, $location, $log, $timeout) {
  $scope.$alert = false;
  var params = new URLSearchParams(document.location.search.replace(/^\?/, ''));
  $scope.datetime = params.get('datetime');
  $scope.id = params.get('id');
  $scope.peer = params.get('peer');
  if(!$scope.datetime || (!$scope.id && !$scope.peer)) {
    console.log($scope);
    $scope.$alert = "The link is corrupt: ";
    return;
  }

  $scope.startTime = new Date($scope.datetime);
  $scope.countdown = ($scope.startTime.getTime() - (new Date()).getTime())/1000;

  $scope.setupAggregationStart = function() {
    var timeout = ($scope.startTime.getTime() - (new Date()).getTime());
    if (timeout < 0) timeout = 0;

    $timeout(function() {
      $scope.startAggregation();
    }, timeout);

  }

  $scope.createNode = function() {
    if(!$scope.id) {
      return;
    }
    $scope.getId = false;
    var invitationParams = new URLSearchParams();
    invitationParams.set('peer', $scope.id);
    invitationParams.set('datetime', $scope.datetime);
    $scope.invitationLink = "participate.html?"+invitationParams.toString();

    var webSocket = require('./web-socket');
    var SignalClient = require('./signal-client');

    $scope.signaller = new SignalClient($scope.id);
    console.log('createNode with id', $scope.id);
    webSocket.on('open', function() {
      $scope.node = new kademlia.Node({
        transport: new WebRTC(new WebRTC.Contact({
          nick: $scope.id
        }), { signaller: $scope.signaller }),
        storage: new kademlia.storage.LocalStorage($scope.id)
      });

      $scope.node.on('connect', function() {
        $log.info("Connection established!");
      });

      // connect to other peer if guest and not host
      if($scope.peer != $scope.id) {
        $scope.node.connect({ nick: $scope.peer }, function(err) {
          if(err) {
            alert(err);
            return;
          }
          $log.info("Connected!");
          $scope.setupAggregationStart();
        });
      } else {
        $log.info("ommit connect");
        $scope.setupAggregationStart();
      }
    });
  }

  if(!$scope.peer) {
    // we are the first peer
    $scope.peer = $scope.id;
    $scope.createNode();
  } else {
    // we are invited
    $scope.getId = true;
  }

  $scope.startAggregation = function() {
    var limit = 1;
    $log.info("startAggregation");
    $scope.aggregator = new Aggregator($scope.node, $scope.node._self);
    $scope.aggregator.processAggregation(function () {
      $scope.$apply(function() {
        $scope.result = $scope.aggregator.resultContainer;
        $scope.participantCount = $scope.aggregator.resultContainer.counter;
        var key = $scope.aggregator.resultContainer.iD;
        $scope.node._router.findNode(key, function(err, contacts) {
          if(err) {
            $log.error(err);
            return;
          }
          contacts.push($scope.node._self);
          $log.info("contacts", contacts);
          var winner = contacts.map(function addDistance(contact) {
            return {
              contact: contact,
              distance: kademlia.utils.getDistance(contact.nodeID, key)
            };
          }).sort(function sortKeysByDistance(a, b) {
            return kademlia.utils.compareKeys(a.distance, b.distance);
          }).splice(0, limit).map(function pluckContact(c) {
            return c.contact;
          });
          $log.info("winner", winner);
          $scope.$apply(function() {
            $scope.ownID = $scope.node._self.nick + " (" + $scope.node._self.nodeID + ", distance = " + Math.log2(kademlia.utils.getDistance(key, $scope.node._self.nodeID).readUIntBE(0,kademlia.constants.B / 8)) + ")";
            $scope.winner = winner[0].nick + " (" + winner[0].nodeID + ", distance = " + Math.log2(kademlia.utils.getDistance(key, winner[0].nodeID).readUIntBE(0,kademlia.constants.B / 8)) + ")";
          });
        });
      });
    });
  }

});
